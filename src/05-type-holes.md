## Haskell Development Techniques { data-transition="fade none" }

###### Type holes 

<pre><code class="language-haskell hljs" data-trim data-noescape>
-- produce any working function
problem1 :: (b -> c) -> (a -> b) -> (a -> c)
problem1 = error "remove this error"
</code></pre>

* Suppose we are given this problem to solve
* Currently, this code compiles, but will throw an exception if it is run
* We are tasked with producing <em>any</em> working function
* That is, any function that satisfies the given type

---

## Haskell Development Techniques { data-transition="fade none" }

###### Type holes

<pre><code class="language-haskell hljs" data-trim data-noescape>
problem1 :: (b -> c) -> (a -> b) -> (a -> c)
problem1 = _typehole
</code></pre>

* We first <code>:load</code> this code into GHCi
* Next we specify a type hole in the place of our solution, by putting a placeholder that begins with an _underscore
* Then we <code>:reload</code>

---

## Haskell Development Techniques { data-transition="fade none" }

###### Type holes

<pre><code class="language-haskell hljs" data-trim data-noescape>
Found hole: _typehole :: (b -> c) -> (a -> b) -> a -> c
</code></pre>

* This tells us that we need a <em>function</em> with this type in that position
* As a good general rule, whenever we see we need a function
  * we write a lambda expression
  * The lambda expression returns another type hole
* <code>&bsol;aname -> _anothertypehole</code>

---

## Haskell Development Techniques { data-transition="fade none" }

###### Type holes

<pre><code class="language-haskell hljs" data-trim data-noescape>
problem1 :: (b -> c) -> (a -> b) -> (a -> c)
problem1 = \aname -> _anothertypehole
</code></pre>

<pre><code class="language-haskell hljs" data-trim data-noescape>
<mark>Prelude&gt;</mark> :reload
Found hole: _anothertypehole :: (a -> b) -> a -> c
</code></pre>

---

## Haskell Development Techniques { data-transition="fade none" }

###### Type holes

<pre><code class="language-haskell hljs" data-trim data-noescape>
problem1 :: (b -> c) -> (a -> b) -> (a -> c)
problem1 = \aname1 -> \aname2 -> \aname3 -> _typehole
</code></pre>

<div style="text-align:center; font-size:x-large">We just keep writing a lambda expression, until we no longer need a function</div>

---

## Haskell Development Techniques { data-transition="fade none" }

###### Type holes

<pre><code class="language-haskell hljs" data-trim data-noescape>
problem1 :: (b -> c) -> (a -> b) -> (a -> c)
problem1 = \aname1 -> \aname2 -> \aname3 -> _typehole
</code></pre>

<pre><code class="language-haskell hljs" data-trim data-noescape>
Found hole: _typehole :: c
Relevant bindings include
  aname3 :: a
  aname2 :: a -> b
  aname1 :: b -> c
</code></pre>

---

## Haskell Development Techniques { data-transition="fade none" }

###### Type holes

<pre><code class="language-haskell hljs" data-trim data-noescape>
Found hole: _typehole :: c
Relevant bindings include
  aname3 :: a
  aname2 :: a -> b
  aname1 :: b -> c
</code></pre>

* We come to the determination that we can produce a value of the type <code>c</code> if we pass an argument to <code>aname1</code>
* So we pursue the solution to this point and use a type hole for what we haven't figured out yet

---

## Haskell Development Techniques { data-transition="fade none" }

###### Type holes

<pre><code class="language-haskell hljs" data-trim data-noescape>
problem1 :: (b -> c) -> (a -> b) -> (a -> c)
problem1 = \aname1 -> \aname2 -> \aname3 -> aname1 _hmm
</code></pre>

---

## Haskell Development Techniques { data-transition="fade none" }

###### Type holes

<pre><code class="language-haskell hljs" data-trim data-noescape>
Found hole: _hmm :: b
Relevant bindings include
  aname3 :: a
  aname2 :: a -> b
  aname1 :: b -> c
</code></pre>

* We are closer to a solution, but not quite there yet
* Now we need a <code>b</code> in place of our type hole
* We observe that we can make a <code>b</code> if we can give an argument to <code>aname2</code>

---

## Haskell Development Techniques { data-transition="fade none" }

###### Type holes

<pre style="font-size: x-large"><code class="language-haskell hljs" data-trim data-noescape>
problem1 :: (b -> c) -> (a -> b) -> (a -> c)
problem1 = \aname1 -> \aname2 -> \aname3 -> aname1 (aname2 _rwethereyet)
</code></pre>

---

## Haskell Development Techniques { data-transition="fade none" }

###### Type holes

<pre><code class="language-haskell hljs" data-trim data-noescape>
Found hole: _rwethereyet :: a
Relevant bindings include
  aname3 :: a
  aname2 :: a -> b
  aname1 :: b -> c
</code></pre>

* Now we need a value of the type <code>a</code>
* We quickly observe that <code>aname3</code> has this type

---

## Haskell Development Techniques { data-transition="fade none" }

###### Type holes

<pre style="font-size: x-large"><code class="language-haskell hljs" data-trim data-noescape>
problem1 :: (b -> c) -> (a -> b) -> (a -> c)
problem1 = \aname1 -> \aname2 -> \aname3 -> aname1 (aname2 aname3)
</code></pre>

<div style="text-align:center; font-size:x-large">We arrive at a solution</div>

---

## Haskell Development Techniques { data-transition="fade none" }

###### Type holes

* Type holes are useful when we believe we know a small part of an answer
* We can incrementally piece together a solution by applying type holes
* After we arrive at a solution, we can tidy up the code

---

## Haskell Development Techniques { data-transition="fade none" }

###### Type holes

<pre><code class="language-haskell hljs" data-trim data-noescape>
problem1 :: (b -> c) -> (a -> b) -> (a -> c)
problem1 f g x = f (g x)
</code></pre>

* Incidentally, this particular problem has only <em>one possible answer</em>
* That is, given this type, all solutions will be the same function
* Not all problems will have a unique answer

---

