## Aims { data-transition="fade none" }

<div style="text-align:left; font-size:x-large">At the end of this lecture, the student should be able to:</div>
* Develop a workflow and familiarity with tools for authoring Haskell programs
* Have the ability to create their own Haskell data types and abstractions (type-classes)

---

