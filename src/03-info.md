## Haskell Tools { data-transition="fade none" }

###### <code>:type</code>

<pre><code class="language-haskell hljs" data-trim data-noescape>
<mark>Prelude&gt;</mark> :type "abc"
"abc" :: [Char]
<mark>Prelude&gt;</mark> :type \x -> "abc" ++ x
\x -> "abc" ++ x :: [Char] -> [Char]
</code></pre>

* We have already seen <code>:type</code> at GHCi to determine the type of an <strong>expression</strong>
* We can also use <code>:t</code> for short

---

## Haskell Tools { data-transition="fade none" }

###### <code>:info</code>

<pre><code class="language-haskell hljs" data-trim data-noescape>
<mark>Prelude&gt;</mark> :info []
data [] a = [] | a : [a] -- Defined in &hellip;
<mark>Prelude&gt;</mark> :info Char
data Char = &hellip; -- Defined in &hellip;
instance Eq Char
</code></pre>

* But what if we didn't know anything about <code>[]</code> or <code>Char</code>?
* We can use <code>:info</code> to ask questions about data types, including our own
* We can also use <code>:i</code> for short

---

## Haskell Tools { data-transition="fade none" }

###### <code>:info</code>

<pre><code class="language-haskell hljs" data-trim data-noescape>
<mark>Prelude&gt;</mark> :info Char
data Char = &hellip; -- Defined in &hellip;
instance Eq Char
<mark>Prelude&gt;</mark> :info Eq
class Eq a where &hellip;
</code></pre>

* We see that <code>Char</code> is a data type and has <code>instance Eq</code>
* But what is <code>Eq</code>?
* <code>Eq</code> is a <code>class</code>, which we will learn more about in this lecture

---

## Haskell Tools { data-transition="fade none" }

###### <code>:info</code> is a universal <q>what is the meaning of &hellip; ?</q> tool

---

