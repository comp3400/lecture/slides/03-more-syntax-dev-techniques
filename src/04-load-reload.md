## Haskell Tools { data-transition="fade none" }

###### <code>:load</code>

* We use <code>:load &lt;<em>source-file</em>&gt;</code> to load a source file
* Dependencies of the source file are also compiled
* GHCi will compile the source file so that its values can be inspected
* We can also use <code>:l</code> for short

---

###### <code>:reload</code>

* After having loaded a file, we make edits and wish to reload the file
* <code>:reload</code> will re-compile the source file and report any errors
* We can also use <code>:r</code> for short
* It is a <strong>very good idea</strong> to <code>:reload</code> regularly

---

###### <code>:load!</code> and <code>:reload!</code>

* Sometimes the source file has type-errors or syntax errors
* However, we still wish to load the file to inspect its values
* We can <code>:reload!</code> the file, which will always compile
* Any errors will become warnings
* Evaluating any erroneous expressions will cause a runtime error

---

