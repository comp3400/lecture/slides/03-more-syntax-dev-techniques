## Revision { data-transition="fade none" }

* Recall
  * Haskell enforces a commitment to the Functional Programming thesis
  * Consequently, we can (always) determine program behaviour by expression substitution
  * Haskell type-checks programs, and disallows those that fail to type-check
  * All Haskell functions take exactly one argument
  * Haskell functions may be polymorphic in their type

---

